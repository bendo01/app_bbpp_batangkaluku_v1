'use strict';
// import _ from 'lodash';
import Vue from 'vue';
import Router from 'vue-router';
import PageLanding from '@/components/page/landing';
import Login from '@/components/page/login';
import ForgotPassword from '@/components/page/forgotpassword';
import Register from '@/components/page/register';
import Training from '@/components/page/training';
import AuthorizationAbilityIndex from '@/components/authorization/ability/index';
import AuthorizationRoleIndex from '@/components/authorization/role/index';
import AuthorizationUserIndex from '@/components/authorization/user/index';
import LocationContinentIndex from '@/components/location/continent/index';
import LocationCountryIndex from '@/components/location/country/index';
import LocationCountryShow from '@/components/location/country/show';
import LocationProvinceIndex from '@/components/location/province/index';
import LocationProvinceShow from '@/components/location/province/show';
import LocationRegencyIndex from '@/components/location/regency/index';
import LocationRegencyShow from '@/components/location/regency/show';
import LocationRegencyTypeIndex from '@/components/location/regencyType/index';
import LocationRegionIndex from '@/components/location/region/index';
import LocationSubDistrictIndex from '@/components/location/subDistrict/index';
import LocationSubDistrictShow from '@/components/location/subDistrict/show';
import LocationVillageIndex from '@/components/location/village/index';
import LocationVillageShow from '@/components/location/village/show';
import InstitutionCurrentInstitutionIndex from '@/components/institution/current/institution/index';
import InstitutionCurrentUnitIndex from '@/components/institution/current/unit/index';
import InstitutionMasterInstitutionIndex from '@/components/institution/master/institution/index';
import InstitutionMasterInstitutionCreate from '@/components/institution/master/institution/create';
import InstitutionMasterInstitutionEdit from '@/components/institution/master/institution/edit';
import InstitutionMasterInstitutionShow from '@/components/institution/master/institution/show';
import InstitutionReferenceCategoryIndex from '@/components/institution/reference/category/index';
import InstitutionReferenceTypeIndex from '@/components/institution/reference/type/index';
import ModuleHumanResourceMasterEmployeeIndex from '@/components/module/humanResource/master/employee/index';
import ModuleHumanResourceMasterEmployeeShow from '@/components/module/humanResource/master/employee/show';
import ModuleHumanResourceReferenceContractTypeIndex from '@/components/module/humanResource/reference/contractType/index';
import ModuleHumanResourceReferenceResignStatusIndex from '@/components/module/humanResource/reference/resignStatus/index';
import ModuleHumanResourceReferenceStatusIndex from '@/components/module/humanResource/reference/status/index';
import ModuleInventoryMasterCatalogIndex from '@/components/module/inventory/master/catalog/index';
import ModuleInventoryReferenceConditionIndex from '@/components/module/inventory/reference/condition/index';
import ModuleInventoryReferenceTypeIndex from '@/components/module/inventory/reference/type/index';
import ModuleInventoryTransactionPlacementIndex from '@/components/module/inventory/transaction/placement/index';
import ModuleTrainingMasterCourseIndex from '@/components/module/training/master/course/index';
import ModuleTrainingMasterCourseCreate from '@/components/module/training/master/course/create';
import ModuleTrainingMasterCourseEdit from '@/components/module/training/master/course/edit';
import ModuleTrainingReferenceFacilitatorIndex from '@/components/module/training/reference/facilitator/index';
import ModuleTrainingReferenceFacilitatorCreate from '@/components/module/training/reference/facilitator/create';
import ModuleTrainingReferenceFacilitatorEdit from '@/components/module/training/reference/facilitator/edit';
import ModuleTrainingReferenceTrainingPackageIndex from '@/components/module/training/reference/trainingPackage/index';
import ModuleTrainingReferenceTrainingPackageCreate from '@/components/module/training/reference/trainingPackage/create';
import ModuleTrainingReferenceTrainingPackageEdit from '@/components/module/training/reference/trainingPackage/edit';
import ModuleTrainingReferenceLearningMaterialCategoryIndex from '@/components/module/training/reference/learningMaterial/category/index';
import ModuleTrainingReferenceLearningMaterialCategoryCreate from '@/components/module/training/reference/learningMaterial/category/create';
import ModuleTrainingReferenceLearningMaterialCategoryEdit from '@/components/module/training/reference/learningMaterial/category/edit';
import ModuleTrainingReferenceLearningMaterialCategoryEditDescription from '@/components/module/training/reference/learningMaterial/category/editDescription';
import ModuleTrainingReferenceLearningMaterialMethodIndex from '@/components/module/training/reference/learningMaterial/method/index';
import ModuleTrainingReferenceLearningMaterialMethodCreate from '@/components/module/training/reference/learningMaterial/method/create';
import ModuleTrainingReferenceLearningMaterialMethodEdit from '@/components/module/training/reference/learningMaterial/method/edit';
import ModuleTrainingTransactionTrainingClassIndex from '@/components/module/training/transaction/trainingClass/index';
import ModuleTrainingTransactionTrainingClassShow from '@/components/module/training/transaction/trainingClass/show';
import ModuleTrainingTransactionTrainingClassShowMember from '@/components/module/training/transaction/trainingClass/showMember';
import ModuleTrainingTransactionTrainingClassCreate from '@/components/module/training/transaction/trainingClass/create';
import ModuleTrainingTransactionTrainingClassEdit from '@/components/module/training/transaction/trainingClass/edit';
import ModuleTrainingTransactionTrainingClassEditMember from '@/components/module/training/transaction/trainingClass/editMember';
import ModuleTrainingTransactionTrainingClassEditFacilitator from '@/components/module/training/transaction/trainingClass/editFacilitator';
import PersonMasterIndividualIndex from '@/components/person/master/individual/index';
import PersonMasterIndividualShow from '@/components/person/master/individual/show';
import PersonReferenceBiodataMaritalStatusIndex from '@/components/person/reference/biodata/maritalStatus/index';
import PersonReferenceBiodataGenderIndex from '@/components/person/reference/biodata/gender/index';
import PersonReferenceBiodataIdentificationTypeIndex from '@/components/person/reference/biodata/identificationType/index';
import PersonReferenceBiodataReligionIndex from '@/components/person/reference/biodata/religion/index';
import PageNotFound from '@/components/page/40x';
import UserProfile from '@/components/user/profile';
import UserDashboard from '@/components/user/dashboard';
import store from '../store/index.js';
Vue.use(Router);

const router = new Router({
    routes: [
        {
            path: '/',
            name: 'landing-page',
            component: PageLanding
        },
        {
            path: '/dashboard',
            name: 'user-dashboard',
            component: UserDashboard,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/userProfile',
            name: 'user-profile',
            component: UserProfile
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/forgot-password',
            name: 'forgot-password',
            component: ForgotPassword
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/training/:categoryId',
            name: 'training',
            component: Training,
            props: true
        },
        {
            path: '/authorization/ability/index',
            name: 'authorization-ability-index',
            component: AuthorizationAbilityIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/authorization/role/index',
            name: 'authorization-role-index',
            component: AuthorizationRoleIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/authorization/user/index',
            name: 'authorization-user-index',
            component: AuthorizationUserIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/institution/current/institution/index',
            name: 'institution-current-institution-index',
            component: InstitutionCurrentInstitutionIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/institution/current/unit/index',
            name: 'institution-current-unit-index',
            component: InstitutionCurrentUnitIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/institution/master/institution/index',
            name: 'institution-master-institution-index',
            component: InstitutionMasterInstitutionIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/institution/master/institution/create',
            name: 'institution-master-institution-create',
            component: InstitutionMasterInstitutionCreate,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/institution/master/institution/:institutionId/edit',
            name: 'institution-master-institution-edit',
            component: InstitutionMasterInstitutionEdit,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/institution/master/institution/:institutionId',
            name: 'institution-master-institution-show',
            component: InstitutionMasterInstitutionShow,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/institution/reference/category/index',
            name: 'institution-reference-category-index',
            component: InstitutionReferenceCategoryIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/institution/reference/type/index',
            name: 'institution-reference-type-index',
            component: InstitutionReferenceTypeIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/location/continent/index',
            name: 'location-continent-index',
            component: LocationContinentIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/location/country/index',
            name: 'location-country-index',
            component: LocationCountryIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/location/country/:countryId',
            name: 'location-country-show',
            component: LocationCountryShow,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/location/province/index',
            name: 'location-province-index',
            component: LocationProvinceIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/location/province/:provinceId',
            name: 'location-province-show',
            component: LocationProvinceShow,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/location/regency/index',
            name: 'location-regency-index',
            component: LocationRegencyIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/location/regency/:regencyId',
            name: 'location-regency-show',
            component: LocationRegencyShow,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/location/regencyType/index',
            name: 'location-regencyType-index',
            component: LocationRegencyTypeIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/location/region/index',
            name: 'location-region-index',
            component: LocationRegionIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/location/subDistrict/index',
            name: 'location-subDistrict-index',
            component: LocationSubDistrictIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/location/subDistrict/:subDistrictId',
            name: 'location-subDistrict-show',
            component: LocationSubDistrictShow,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/location/village/index',
            name: 'location-village-index',
            component: LocationVillageIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/location/village/:villageId',
            name: 'location-village-show',
            component: LocationVillageShow,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/humanResource/master/employee',
            name: 'module-humanResource-master-employee-index',
            component: ModuleHumanResourceMasterEmployeeIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/humanResource/master/employee/show',
            name: 'module-humanResource-master-employee-show',
            component: ModuleHumanResourceMasterEmployeeShow,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/humanResource/reference/contractType/index',
            name: 'module-humanResource-reference-contractType-index',
            component: ModuleHumanResourceReferenceContractTypeIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/humanResource/reference/resignStatus/index',
            name: 'module-humanResource-reference-resignStatus-index',
            component: ModuleHumanResourceReferenceResignStatusIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/humanResource/reference/status/index',
            name: 'module-humanResource-reference-status-index',
            component: ModuleHumanResourceReferenceStatusIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/inventory/master/catalog/index',
            name: 'module-inventory-master-catalog-index',
            component: ModuleInventoryMasterCatalogIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/inventory/reference/condition/index',
            name: 'module-inventory-reference-condition-index',
            component: ModuleInventoryReferenceConditionIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/inventory/reference/type/index',
            name: 'module-inventory-reference-type-index',
            component: ModuleInventoryReferenceTypeIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/inventory/transaction/placement/index',
            name: 'module-inventory-transaction-placement-index',
            component: ModuleInventoryTransactionPlacementIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/master/course/index',
            name: 'module-training-master-course-index',
            component: ModuleTrainingMasterCourseIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/master/course/create',
            name: 'module-training-master-course-create',
            component: ModuleTrainingMasterCourseCreate,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/master/course/:courseId/edit',
            name: 'module-training-master-course-edit',
            component: ModuleTrainingMasterCourseEdit,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/reference/facilitator/index',
            name: 'module-training-reference-facilitator-index',
            component: ModuleTrainingReferenceFacilitatorIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/reference/facilitator/create',
            name: 'module-training-reference-facilitator-create',
            component: ModuleTrainingReferenceFacilitatorCreate,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/reference/facilitator/:facilitatorId/edit',
            name: 'module-training-reference-facilitator-edit',
            component: ModuleTrainingReferenceFacilitatorEdit,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/reference/trainingPackage/index',
            name: 'module-training-reference-trainingPackage-index',
            component: ModuleTrainingReferenceTrainingPackageIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/reference/trainingPackage/create',
            name: 'module-training-reference-trainingPackage-create',
            component: ModuleTrainingReferenceTrainingPackageCreate,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/reference/trainingPackage/:trainingPackageId/edit',
            name: 'module-training-reference-trainingPackage-edit',
            component: ModuleTrainingReferenceTrainingPackageEdit,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/reference/learningMaterial/category/index',
            name: 'module-training-reference-learningMaterial-category-index',
            component: ModuleTrainingReferenceLearningMaterialCategoryIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/reference/learningMaterial/category/create',
            name: 'module-training-reference-learningMaterial-category-create',
            component: ModuleTrainingReferenceLearningMaterialCategoryCreate,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/reference/learningMaterial/category/:categoryId/editDescription',
            name: 'module-training-reference-learningMaterial-category-edit-description',
            component: ModuleTrainingReferenceLearningMaterialCategoryEditDescription,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/reference/learningMaterial/category/:categoryId/edit',
            name: 'module-training-reference-learningMaterial-category-edit',
            component: ModuleTrainingReferenceLearningMaterialCategoryEdit,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/reference/learningMaterial/method/index',
            name: 'module-training-reference-learningMaterial-method-index',
            component: ModuleTrainingReferenceLearningMaterialMethodIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/reference/learningMaterial/method/create',
            name: 'module-training-reference-learningMaterial-method-create',
            component: ModuleTrainingReferenceLearningMaterialMethodCreate,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/reference/learningMaterial/method/:methodId/edit',
            name: 'module-training-reference-learningMaterial-method-edit',
            component: ModuleTrainingReferenceLearningMaterialMethodEdit,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/transaction/trainingClass/index',
            name: 'module-training-transaction-trainingClass-index',
            component: ModuleTrainingTransactionTrainingClassIndex
        },
        {
            path: '/module/training/transaction/trainingClass/create',
            name: 'module-training-transaction-trainingClass-create',
            component: ModuleTrainingTransactionTrainingClassCreate,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/transaction/trainingClass/:trainingClassId/editFacilitator',
            name: 'module-training-transaction-trainingClass-editFacilitator',
            component: ModuleTrainingTransactionTrainingClassEditFacilitator,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/transaction/trainingClass/:trainingClassId/editMember',
            name: 'module-training-transaction-trainingClass-editMember',
            component: ModuleTrainingTransactionTrainingClassEditMember,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/transaction/trainingClass/:trainingClassId/edit',
            name: 'module-training-transaction-trainingClass-edit',
            component: ModuleTrainingTransactionTrainingClassEdit,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/module/training/transaction/trainingClass/:trainingClassId/showMember',
            name: 'module-training-transaction-trainingClass-show-member',
            component: ModuleTrainingTransactionTrainingClassShowMember,
            props: true
        },
        {
            path: '/module/training/transaction/trainingClass/:trainingClassId',
            name: 'module-training-transaction-trainingClass-show',
            component: ModuleTrainingTransactionTrainingClassShow,
            props: true
        },
        {
            path: '/person/master/individual/index',
            name: 'person-master-individual-index',
            component: PersonMasterIndividualIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/person/master/individual/:individualId',
            name: 'person-master-individual-show',
            component: PersonMasterIndividualShow,
            props: true,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/person/reference/biodata/gender/index',
            name: 'person-reference-biodata-gender-index',
            component: PersonReferenceBiodataGenderIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/person/reference/biodata/identificationType/index',
            name: 'person-reference-biodata-identificationType-index',
            component: PersonReferenceBiodataIdentificationTypeIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/person/reference/biodata/maritalStatus/index',
            name: 'person-reference-biodata-maritalStatus-index',
            component: PersonReferenceBiodataMaritalStatusIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '/person/reference/biodata/religion/index',
            name: 'person-reference-biodata-religion-index',
            component: PersonReferenceBiodataReligionIndex,
            beforeEnter: (to, from, next) => {
                if (store.state.isAuthenticated) {
                    if (store.state.authUser.is_admin) {
                        next();
                    } else {
                        next('/userProfile');
                    }
                } else {
                    next('/login');
                }
            }
        },
        {
            path: '*',
            component: PageNotFound
        }
    ],
    linkActiveClass: 'is-active'
});
router.beforeEach((to, from, next) => {
    store.dispatch('closeSideMenu');
    document.documentElement.classList.remove('is-clipped');
    next();
});

export default router;
