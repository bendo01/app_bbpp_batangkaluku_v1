// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
'use strict';

import Vue from 'vue';
import App from './App';
import Buefy from 'buefy';
import store from './store';
import router from './router';
import VeeValidate from 'vee-validate';
import VueQuillEditor from 'vue-quill-editor';
import moment from 'moment';

/*
import { ApolloClient, createNetworkInterface } from 'apollo-client';
import VueApollo from 'vue-apollo';
import {queryUrl} from './config.js';

const apolloClient = new ApolloClient({
    networkInterface: createNetworkInterface({
        uri: queryUrl,
        transportBatching: true
    }),
    connectToDevTools: true
});
*/
// Install the vue plugin
// With the apollo client instance
// Vue.use(Buefy, VueApollo);
Vue.use(Buefy, {
    defaultDateFormatter: (date) => {
        return moment(date).format('DD-MM-YYYY');
    },
    defaultDateParser: (date) => {
        return moment(date, 'DD-MM-YYYY');
    }
});
Vue.use(VeeValidate);
Vue.use(VueQuillEditor);
// Vue.config.productionTip = false;
/*
const apolloProvider = new VueApollo({
    defaultClient: apolloClient
});
*/
/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    // apolloProvider,
    template: '<App/>',
    components: { App }
});
