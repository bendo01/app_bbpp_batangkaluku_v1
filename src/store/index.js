'use strict';
import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import {clientId as authClientId, clientSecret as authClientSecret} from '../config.js';

Vue.use(Vuex);
const store = new Vuex.Store({
    strict: true,
    plugins: [createPersistedState()],
    state: {
        clientId: authClientId,
        clientSecret: authClientSecret,
        isMenuOpen: false,
        isShowLoader: false,
        loaderMessage: 'Proses Data',
        isErrorMessageReceivedFromServer: false,
        authUser: null,
        expiresIn: null,
        expiredOn: null,
        tokenType: null,
        accessToken: null,
        refreshToken: null,
        isAuthenticated: false,
        isNotification: false,
        notificationMessage: {}
    },
    getters: {
        isSideMenuOpened (state) {
            return state.isMenuOpen;
        },
        isShowLoader (state) {
            return state.isShowLoader;
        },
        loaderMessage (state) {
            return state.loaderMessage;
        },
        isErrorMessageReceivedFromServer (state) {
            return state.isErrorMessageReceivedFromServer;
        },
        authUser (state) {
            return state.authUser;
        },
        expiresIn (state) {
            return state.expiresIn;
        },
        expiredOn (state) {
            return state.expiredOn;
        },
        tokenType (state) {
            return state.tokenType;
        },
        accessToken (state) {
            return state.accessToken;
        },
        refreshToken (state) {
            return state.refreshToken;
        },
        isNotification (state) {
            return state.isNotification;
        },
        isAuthenticated (state) {
            return state.isAuthenticated;
        },
        notificationMessage (state) {
            return state.notificationMessage;
        },
        clientId (state) {
            return state.clientId;
        },
        clientSecret (state) {
            return state.clientSecret;
        }
    },
    mutations: {
        openSideMenu (state) {
            // mutate state
            state.isMenuOpen = true;
        },
        closeSideMenu (state) {
            // mutate state
            state.isMenuOpen = false;
            document.documentElement.classList.remove('is-clipped');
        },
        showLoader (state) {
            // mutate state
            state.isShowLoader = true;
        },
        hideLoader (state) {
            // mutate state
            state.isShowLoader = false;
        },
        setLoaderMessage (state, message) {
            // mutate state
            state.loaderMessage = message;
        },
        setErrorMessageReceivedFromServer (state) {
            // mutate state
            state.isErrorMessageReceivedFromServer = true;
        },
        clearErrorMessageReceivedFromServer (state) {
            // mutate state
            state.isErrorMessageReceivedFromServer = false;
        },
        setAuthenticate (state) {
            // mutate state
            state.isAuthenticated = true;
        },
        clearAuthenticate (state) {
            // mutate state
            state.isAuthenticated = false;
        },
        setAuthUser (state, authUser) {
            // mutate state
            state.authUser = authUser;
        },
        clearAuthUser (state) {
            // mutate state
            state.authUser = null;
        },
        setExpiresIn (state, expiresIn) {
            // mutate state
            state.expiresIn = expiresIn;
            state.expiredOn = expiresIn + Date.now();
        },
        clearExpiresIn (state) {
            // mutate state
            state.expiresIn = null;
            state.expiredOn = null;
        },
        setTokenType (state, tokenType) {
            // mutate state
            state.tokenType = tokenType;
        },
        clearTokenType (state) {
            // mutate state
            state.tokenType = null;
        },
        setAccessToken (state, accessToken) {
            // mutate state
            state.accessToken = accessToken;
        },
        clearAccessToken (state) {
            // mutate state
            state.accessToken = null;
        },
        setRefreshToken (state, refreshToken) {
            // mutate state
            state.refreshToken = refreshToken;
        },
        clearRefreshToken (state) {
            // mutate state
            state.refreshToken = null;
        },
        setNotification (state, notificationMessage) {
            state.isNotification = true;
            state.notificationMessage = notificationMessage;
        },
        clearNotification (state) {
            state.isNotification = false;
            state.notificationMessage = {};
        },
        setClientId (state, clientId) {
            state.isNotification = false;
            state.clientId = clientId;
        },
        setClientSecret (state, clientSecret) {
            state.isNotification = false;
            state.clientSecret = clientSecret;
        },
        clearClient (state) {
            state.isNotification = false;
            state.clientSecret = null;
            state.clientId = null;
        }
    },
    actions: {
        showLoader (context) {
            context.commit('showLoader');
        },
        openSideMenu (context) {
            context.commit('openSideMenu');
        },
        closeSideMenu (context) {
            context.commit('closeSideMenu');
        },
        hideLoader (context) {
            context.commit('hideLoader');
            context.commit('closeSideMenu');
        },
        setErrorMessageReceivedFromServer (context) {
            context.commit('setErrorMessageReceivedFromServer');
        },
        clearErrorMessageReceivedFromServer (context) {
            context.commit('clearErrorMessageReceivedFromServer');
        },
        setAuthenticate (context) {
            context.commit('setAuthenticate');
        },
        clearAuthenticate (context) {
            context.commit('clearAuthenticate');
        },
        setExpiresIn (context, expiresIn) {
            context.commit('setExpiresIn', expiresIn);
        },
        clearExpiresIn (context) {
            context.commit('clearExpiresIn');
        },
        setTokenType (context, tokenType) {
            context.commit('setTokenType', tokenType);
        },
        clearTokenType (context) {
            context.commit('clearTokenType');
        },
        setAccessToken (context, accessToken) {
            context.commit('setAccessToken', accessToken);
        },
        clearAccessToken (context) {
            context.commit('clearAccessToken');
        },
        setRefreshToken (context, refreshToken) {
            context.commit('setRefreshToken', refreshToken);
        },
        clearRefreshToken (context) {
            context.commit('clearRefreshToken');
        },
        setRoles (context, roles) {
            context.commit('setRoles', roles);
        },
        clearRoles (context) {
            context.commit('clearRoles');
        },
        setAuthUser (context, authUser) {
            context.commit('setAuthUser', authUser);
        },
        clearAuthUser (context) {
            context.commit('clearAuthUser');
        },
        setNotification (context, notificationMessage) {
            context.commit('setNotification', notificationMessage);
        },
        clearNotification (context) {
            context.commit('clearNotification');
        },
        setClientId (context, clientId) {
            context.commit('setClientId', clientId);
        },
        setClientSecret (context, clientSecret) {
            context.commit('setClientSecret', clientSecret);
        },
        clearClient (context) {
            context.commit('clearClient');
        }
    }
});
export default store;
